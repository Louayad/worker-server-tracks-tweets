using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Tweetinvi;
using System.Configuration;
using Tweetinvi.Models;
using MongoDB.Bson;
using MongoDB.Driver;
namespace WorkerService1
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        int i = 0;
       
        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }
        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("The service has been started...");
            return base.StartAsync(cancellationToken);
        }


        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("The service has been stopped...");
            return base.StopAsync(cancellationToken);
        }
        
        public  void  Tweet()
        {
            //Connect to Twitter API through twitter application (Consumer key..)
            Auth.SetUserCredentials("vktSUwfTGssEWlShha6gNTUxk", "8I9KTqQ945W9MwiGCKTx85zQAOwydpg8Ad0YH4OpOLJAkXMCJi", "1202014106708041730-uZwOaRDxNOq79FiNKaNcVexIJ2AbRg", "4tmEU82uTj7cwqGCQuWgGnAOVfUN3sllyJybByul1rLGA");
            //Connect to Database
            var client = new MongoClient("mongodb://louayayadi:louaylouay@cluster0-shard-00-00-6qbjh.mongodb.net:27017,cluster0-shard-00-01-6qbjh.mongodb.net:27017,cluster0-shard-00-02-6qbjh.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
            //Get database
            var Database = client.GetDatabase("testdb");
            //Get collection to work on
            var Tweets = Database.GetCollection<BsonDocument>("tweets");
            
            //Tweet to Track
            string query = "startup";
    
             
            // Create the Stream
            var stream = Stream.CreateFilteredStream();

            // Keywords to Track
            stream.AddTrack("#"+query);
            
            //Convert strem  variable to BsonDocument
            stream.ToBsonDocument();
            // Limit to English 
            stream.AddTweetLanguageFilter(LanguageFilter.English);

            // Message so you know its running
            Console.WriteLine("I am listening to Twitter");

            // Called when a tweet maching the tracker is produced
            //i=30 is the Stopping Condition, finding 30 tweets matching your Tracker
            while (i<30)
            {
                stream.MatchingTweetReceived +=async (sender, arguments) =>
                {
                    //Write the tweet Text and Id
                    Console.WriteLine("Tweet= " + arguments.Tweet.Text);
                    Console.WriteLine("Id= " + arguments.Tweet.Id);
                    //Increment counter
                    i += 1;
                    if (i == 31)
                    {
                        Console.WriteLine("\n limit reached \n");
                        stream.StopStream();
                      
                        //Stop service when having 30 tweets
                        System.Environment.Exit(0);


                    }

                    
                    Console.WriteLine("i= " + i);

                    var doc = new BsonDocument {
                    {"Text",arguments.Tweet.Text },{"_id",arguments.Tweet.Id}
                                               };
                    //To test existance of a tweet text in the database collection
                    var filter = Builders<BsonDocument>.Filter.Eq("Text", arguments.Tweet.Text);
                    var Text = Tweets.Find(filter).FirstOrDefault();
                    
                    if (Text == null)
                        await Tweets.InsertOneAsync(doc);
                    //Decrement the counter once a Tweet existed before in the database so it won'b be added again
                    else i -= 1;
                    
                    

                };

               
                
                
                    stream.StartStreamMatchingAllConditions();
                    
                
            }
           
           
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                
                await Task.Run(() => Tweet());
                await Task.Delay(000, stoppingToken);
               

                


            }
        }
    }
}
